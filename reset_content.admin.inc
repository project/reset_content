<?php

/**
 * @file
 * Managment pages for Reset content module.
 */

/**
 * Form builder.
 */
function reset_content_settings_form($form, &$form_state) {

  $form['reset_content_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Reset Content'),
    '#description' => t('This just to make sure that you are aware of what this module will do. After enable it from here more fields will be shown.'),
    '#default_value' => variable_get('reset_content_status', FALSE),
  );

  $form['reset_content_interval'] = array(
    '#type' => 'select',
    '#title' => t('Interval'),
    '#description' => t('Set the interval time to reset the content using cronjob.'),
    '#default_value' => variable_get('reset_content_interval', FALSE),
    '#options' => array(
      60 => t('1 minute'),
      3600 => t('1 hour'),
      (86400 * 1) => t('1 day'),
      (86400 * 2) => t('2 day'),
      (86400 * 3) => t('3 day'),
      (86400 * 4) => t('4 day'),
      (86400 * 5) => t('5 day'),
      (86400 * 6) => t('6 day'),
      (86400 * 7) => t('7 day'),
    ),
  );

  // Check if it is enabled.
  if (variable_get('reset_content_status', FALSE)) {
    $last_snapshot = variable_get('reset_content_last_snapshot', FALSE);
    if ($last_snapshot) {
      $last_snapshot = format_date($last_snapshot, 'short');
    }
    $form['reset_content_snapshot_button'] = array(
      '#type' => 'submit',
      '#value' => t('Take snapshot now'),
      '#suffix' => t('Last snapshot: @last', array('@last' => $last_snapshot)),
      '#submit' => array('reset_content_settings_form_take_snapshop_submit'),
    );
  }

  return system_settings_form($form);
}

/**
 * Take snapshot now. Callback for button from settings form.
 */
function reset_content_settings_form_take_snapshop_submit($form, &$form_state) {
  variable_set('reset_content_last_snapshot', time());

  // We will fetch the last nid from database for all these entities:
  // - Nodes.
  // - Taxonomy vocabularies.
  // - Taxonomy terms.
  // - Users.

  // Fetch the last node id.
  $last_nid = db_query_range("SELECT nid FROM {node} ORDER BY nid DESC", 0, 1)->fetchField();
  if (!is_numeric($last_nid))
    $last_nid = 0;

  // Fetch the last taxonomy term id.
  $last_tid = db_query_range("SELECT tid FROM {taxonomy_term_data} ORDER BY tid DESC", 0, 1)->fetchField();
  if (!is_numeric($last_tid))
    $last_tid = 0;

  // Fetch the last vocabulary id.
  $last_vid = db_query_range("SELECT vid FROM {taxonomy_vocabulary} ORDER BY vid DESC", 0, 1)->fetchField();
  if (!is_numeric($last_vid))
    $last_vid = 0;

  // Fetch the last user id.
  $last_uid = db_query_range("SELECT uid FROM {users} ORDER BY uid DESC", 0, 1)->fetchField();
  if (!is_numeric($last_uid))
    $last_uid = 0;

  // Prepare the array.
  $last_ids = array();
  $last_ids['nid'] = $last_nid;
  $last_ids['tid'] = $last_tid;
  $last_ids['vid'] = $last_vid;
  $last_ids['uid'] = $last_uid;

  // Save last ids into one variable.
  variable_set('reset_content_last_ids', $last_ids);

  // Reset the interval timer.
  $interval = variable_get('reset_content_interval', 3600);
  variable_set('reset_content_next_execution', time() + $interval);
}
