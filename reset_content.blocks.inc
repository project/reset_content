<?php

/**
 * @file
 * Define blocks for reset content module.
 */

/**
 * Implements hook_block_info().
 */
function reset_content_block_info() {
  $blocks = array();
  // Block to show reset content timer.
  $blocks['reset_content_timer'] = array(
    'info' => t('Reset content timer'),
    'status' => FALSE,
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function reset_content_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'reset_content_timer':
      $block['subject'] = t('Reset content timer');
      $block['content'] = reset_content_timer_block_content();
      break;
  }
  return $block;
}

/**
 * Content for reset content timer block.
 */
function reset_content_timer_block_content() {
  $next_execution = variable_get('reset_content_next_execution', 0);
  if ($next_execution > time()) {
    $next_execution_time = $next_execution - time();
  }
  else {
    $next_execution_time = 0;
  }

  // Attach our custom css file.
  drupal_add_css(drupal_get_path('module', 'reset_content') . '/css/reset-content.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));

  $next_execution_time = format_interval($next_execution_time);
  return theme('reset_content_timer_template', array('next_execution_time' => $next_execution_time));
}
