Reset Content
=============

This module will help you to take something like a snapshot. And whenever you want you can remove all new (Contents) has been added after this snapshot.
We don't have any plan to support taking a snapshot of anything are not an entity!
So, Views and variables will not be reset.

Roadmap:
* [ ] Set a timer to reset content. (Useful for demo sites).
* [ ] Allow other module to alter the reset process using custom hook.
* [ ] Support taking more than one snapshot and save these snapshots as titles.
