<?php

/**
 * @file
 * Helper functions for Reset content module.
 */

/**
 * Delete all nodes starting from the given nid.
 *
 * The given nid will be deleted.
 */
function reset_content_delete_nodes($starting_nid) {
  $nids = db_query("SELECT * FROM {node} WHERE nid >= :nid", array(':nid' => $starting_nid))->fetchAll();
  $count = 0;
  if (is_array($nids) && (count($nids) > 0)) {
    foreach ($nids as $nid) {
      // Delete node.
      node_delete($nid->nid);
      $count++;
    }
  }

  return $count;
}

/**
 * Delete all taxonomy terms starting from the given tid.
 *
 * The given tid will be deleted.
 */
function reset_content_delete_taxonomy_terms($starting_tid) {
  $tids = db_query("SELECT * FROM {taxonomy_term_data} WHERE tid >= :tid", array(':tid' => $starting_tid))->fetchAll();
  $count = 0;
  if (is_array($tids) && (count($tids) > 0)) {
    foreach ($tids as $tid) {
      // Delete term.
      taxonomy_term_delete($tid->tid);
      $count++;
    }
  }

  return $count;
}

/**
 * Delete all taxonomy vocabularies starting from the given vid.
 *
 * The given vid will be deleted.
 */
function reset_content_delete_taxonomy_vocabularies($starting_vid) {
  $vids = db_query("SELECT * FROM {taxonomy_vocabulary} WHERE vid >= :vid", array(':vid' => $starting_vid))->fetchAll();
  $count = 0;
  if (is_array($vids) && (count($vids) > 0)) {
    foreach ($vids as $vid) {
      // Delete vocabulary.
      taxonomy_vocabulary_delete($vid->vid);
      $count++;
    }
  }

  return $count;
}

/**
 * Delete all users starting from the given uid.
 *
 * The given uid will be deleted.
 */
function reset_content_delete_users($starting_uid) {
  $uids = db_query("SELECT * FROM {users} WHERE uid >= :uid", array(':uid' => $starting_uid))->fetchAll();
  $count = 0;
  if (is_array($uids) && (count($uids) > 0)) {
    foreach ($uids as $uid) {
      // Delete user.
      user_delete($uid->uid);
      $count++;
    }
  }

  return $count;
}
